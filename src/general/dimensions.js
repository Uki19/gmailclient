
export const marginSmall = 2;
export const marginDefault = 10;
export const marginDefaultHalved = marginDefault / 2;
export const marginDefaultDouble = marginDefault * 2;

export const paddingSmall = 2;
export const paddingDefault = 10;
export const paddingDefaultHalved = paddingDefault / 2;
export const paddingDefaultDouble = paddingDefault * 2;

export const borderRadiusSmall = 4;
export const borderRadiusMedium = 10;
export const borderRadiusLarge = 16;
