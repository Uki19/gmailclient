// @flow

import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({
    defaultShadow: {
        shadowColor: "#444",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 2,
    },
});
