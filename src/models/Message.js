import { getRelativeDateString } from "../utils/dateTime";

// @flow

type HeaderItem = {
    name: string;
    value: string;
}

type Headers = {
    from: string;
    subject: string;
    deliveredTo: string;
    to: string;
    date: string;
    sender: string;
}

export type Attachment = {
    mimeType: string;
    filename: string;
    body: AttachmentBody;
}

type AttachmentBody = {
    attachmentId: string;
    size: number;
    data: string;
}

type MessagePart = {
    id: string;
    mimeType: string;
    filename: string;
    body: MessagePartBody | AttachmentBody;
    parts: MessagePart[];
}

type MessagePartBody = {
    size: number;
    data: string;
}

export const LabelIds = {
    unread: "UNREAD",
    sent: "SENT",
    draft: "DRAFT",
    inbox: "INBOX",
};

export default class Message {
    id: string;

    threadId: string;

    snippet: string;

    internalDate: number;

    labelIds: string[];

    isUnread: boolean;

    parts: MessagePart[];

    body: MessagePartBody | AttachmentBody;

    htmlBody: ?MessagePartBody;

    textBody: ?MessagePartBody;

    headers: Headers;

    attachments: Attachment[];

    representableDate: String;

    constructor(json: { [string]: any }) {
        this.id = json.id;
        this.threadId = json.threadId;
        if (json.headers) {
            this.headers = json.headers;
        }
        if (json.snippet) {
            this.snippet = json.snippet;
        }
        if (json.internalDate) {
            this.internalDate = parseInt(json.internalDate, 10);
            this.representableDate = getRelativeDateString(this.internalDate);
        }
        if (json.labelIds) {
            this.labelIds = json.labelIds;
        }
        this.attachments = [];
        this.isUnread = this.labelIds.includes(LabelIds.unread);

        if (json.payload) {
            this.body = json.payload.body;
            this.headers = this.parseHeaders(json.payload.headers);
            this.parseParts(json.payload);
        }
    }

    parseParts = (payload: MessagePart) => {
        if (payload.mimeType === "text/html") {
            this.htmlBody = payload.body;
        }
        if (payload.mimeType === "text/plain") {
            this.textBody = payload.body;
        }
        if (payload.body.attachmentId) {
            this.attachments.push(payload);
        }
        if (!payload.parts) return;
        payload.parts.forEach((part) => {
            this.parseParts(part);
        });
    }

    parseHeaders = (headerItems: HeaderItem[]): Headers => {
        return headerItems.reduce((headers, headerItem) => {
            const replacedName = headerItem.name.replace("-", "");
            const key = replacedName[0].toLowerCase() + replacedName.slice(1);
            return { ...headers, [key]: headerItem.value };
        }, ({}: Headers));
    }

    static fromApiList(jsonList: { [string]: any }[]): Message[] {
        return jsonList.map(json => new Message(json));
    }
}
