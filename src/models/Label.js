// @flow

type LabelType = "user" | "system";

export const LabelTypeEnum = {
    user: "user",
    system: "syste",
};

export default class Label {
    id: string;

    name: string;

    type: LabelType;

    constructor(json: { [string]: any }) {
        this.id = json.id;

        if (json.name) {
            this.name = json.name;
        }

        if (json.type) {
            this.type = json.type;
        }
    }

    static fromApiList(jsonList: { [string]: any }[]): Label[] {
        return jsonList.map(json => new Label(json));
    }
}
