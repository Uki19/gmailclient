// @flow

import React from "react";
import { Image, ImageSourcePropType } from "react-native";
import { createBottomTabNavigator, createStackNavigator } from "react-navigation";

import InboxScreen from "../screens/InboxScreen";
import MessageScreen from "../screens/MessageScreen";
import NewMessageScreen from "../screens/NewMessageScreen";
import defaultNavigationOptions from "./navigationOptions";
import { inboxTabIcon, userTabIcon, outboxTabIcon } from "../resources/images";
import { veryLightTangelo, dustyGray, myrtleGreen } from "../general/colors";
import ScreenKeys from "../screens/ScreenKeys";
import ProfileScreen from "../screens/ProfileScreen";
import SentScreen from "../screens/SentScreen";

const InboxTabStackNavigation = createStackNavigator(
    {
        [ScreenKeys.Inbox]: {
            screen: InboxScreen,
            navigationOptions: {
                title: "Inbox",
            },
        },
        [ScreenKeys.Message]: MessageScreen,
    },
    defaultNavigationOptions,
);

const SentTabStackNavigation = createStackNavigator(
    {
        [ScreenKeys.Sent]: {
            screen: SentScreen,
            navigationOptions: {
                title: "Sent",
            },
        },
        [ScreenKeys.Message]: MessageScreen,
    },
    {
        navigationOptions: {
            ...defaultNavigationOptions.navigationOptions,
            headerStyle: {
                backgroundColor: myrtleGreen,
            },
        },
    },
);

const MainTabNavigation = createBottomTabNavigator(
    {
        [ScreenKeys.InboxTab]: {
            screen: InboxTabStackNavigation,
            navigationOptions: {
                title: "Inbox",
                tabBarIcon: ({ tintColor }) => {
                    return getTabIcon(inboxTabIcon, tintColor);
                },
            },
        },
        [ScreenKeys.SentTab]: {
            screen: SentTabStackNavigation,
            navigationOptions: {
                title: "Sent",
                tabBarIcon: ({ tintColor }) => {
                    return getTabIcon(outboxTabIcon, tintColor);
                },
            },
        },
        [ScreenKeys.ProfileTab]: {
            screen: ProfileScreen,
            navigationOptions: {
                title: "Profile",
                tabBarIcon: ({ tintColor }) => {
                    return getTabIcon(userTabIcon, tintColor);
                },
            },
        },
    },
    {
        tabBarOptions: {
            activeTintColor: veryLightTangelo,
            inactiveTintColor: dustyGray,
        },
    },
);

const MainNavigation = createStackNavigator(
    {
        [ScreenKeys.MainTabNavigation]: MainTabNavigation,
        [ScreenKeys.NewMessage]: NewMessageScreen,
    },
    {
        ...defaultNavigationOptions,
        mode: "modal",
        headerMode: "none",
    },
);

const getTabIcon = (source: ImageSourcePropType, tintColor: string) => {
    return <Image source={source} style={{ tintColor }} />;
};

export default MainNavigation;
