// @flow

import { createStackNavigator } from "react-navigation";

import LoginScreen from "../screens/LoginScreen";
import defaultNavigationOptions from "./navigationOptions";

const LoginNavigation = createStackNavigator(
    {
        Login: {
            screen: LoginScreen,
        },
    },
    defaultNavigationOptions,
);

export default LoginNavigation;
