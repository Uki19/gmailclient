// @flow

import { steelBlue } from "../general/colors";

const defaultNavigationOptions = {
    navigationOptions: {
        headerStyle: {
            backgroundColor: steelBlue,
        },
        headerTintColor: "white",
        headerTitleStyle: {
            fontWeight: "bold",
        },
    },
};

export default defaultNavigationOptions;
