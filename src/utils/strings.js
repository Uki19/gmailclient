// @flow

export const isStringEmail = (s: string): boolean => {
    const reg = new RegExp(/^[^@]+@[^@]+\.[^@]+$/);
    return reg.test(s);
};
