// @flow

type User = {
    email: string;
    id: string;
    givenName: string;
    familyName: string;
    photo: string;
    name: string;
};

export type UserInfo = {
    idToken: string;
    accessToken: string | null;
    serverAuthCode: string;
    user: User;
};

export default class CurrentUser {
    static user: UserInfo;

    static get(): UserInfo {
        return this.user;
    }

    static set(user: UserInfo) {
        this.user = user;
    }
}
