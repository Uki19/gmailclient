// @flow

import axios from "axios";
import CurrentUser from "./CurrentUser";

export function setupAxios() {
    const userInfo = CurrentUser.get();
    axios.defaults.baseURL = "https://www.googleapis.com/gmail/v1/users/me";
    if (userInfo.accessToken) {
        axios.defaults.headers.common["Authorization"] = `Bearer ${userInfo.accessToken}`;
    }
}
