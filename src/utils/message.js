// @flow

import { Buffer } from "buffer";

import CurrentUser from "./CurrentUser";

export const getEncodedMessage = (
    to: string,
    subject: string,
    body: string,
): string => {
    const { email } = CurrentUser.get().user;
    const safeSubject = subject.length > 0 ? subject : "(no subject)";
    const message = [
        `From: ${email}`,
        `To: ${to.toLowerCase()}`,
        `Subject: ${safeSubject}`,
        "",
        body,
    ].join("\n");
    const encodedMessage = Buffer.from(message)
        .toString("base64")
        .replace(/\+/g, "-")
        .replace(/\//g, "_")
        .replace(/=+$/, "");
    return encodedMessage;
};
