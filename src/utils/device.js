import { Platform } from "react-native";

// @flow

export const isPlatformIOS = Platform.OS === "ios";
export const isPlatformAndroid = Platform.OS === "android";
