
import { AsyncStorage } from "react-native";

export const Keys = {
    ACCESS_TOKEN: "ACCESS_TOKEN",
};

export const getItem = (key: string): Promise<string | null> => {
    return AsyncStorage.getItem(key);
};

export const setItem = (key: string, value: string): Promise<void> => {
    return AsyncStorage.setItem(key, value);
};