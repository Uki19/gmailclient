
// @flow
import { isPlatformAndroid } from "./device";


export const viewPortScript = `
document.getElementsByTagName('head')[0].innerHTML += '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
`;

export const getScrollHeightScript = `
var threshold = 10;
function onElementHeightChange(elm, callback){
    var lastHeight = document.documentElement.scrollHeight, newHeight;
    (function run(){
        newHeight = document.documentElement.scrollHeight;
        if(Math.abs(lastHeight - newHeight) > threshold) {
            callback();
        }
        lastHeight = newHeight;

        if(elm.onElementHeightChangeTimer) {
            clearTimeout(elm.onElementHeightChangeTimer);
        }

        elm.onElementHeightChangeTimer = setTimeout(run, 200);
    })();
}

onElementHeightChange(document.body, function() {
    sendScrollHeight();
});

function sendScrollHeight() {
    const messageData = {
        name: "SET_SCROLL_HEIGHT",
        payload: {
            scrollHeight: document.documentElement.scrollHeight,
        }
    };
    window.postMessage(JSON.stringify(messageData), "*");
}

(function(){
    if (${isPlatformAndroid}) {
        setTimeout(function(){ sendScrollHeight(); }, 1);
    } else {
        sendScrollHeight();
    }
})();
`;

export const initialScript = `
${viewPortScript}
${getScrollHeightScript}
`;
