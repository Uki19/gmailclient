import moment from "moment";

export const getRelativeDateString = (timestamp: number): string => {
    const date = moment(timestamp).local();
    const now = moment();
    const daysDiff = now.diff(date, "days");

    if (daysDiff < 5) {
        return date.fromNow(true);
    }

    return date.format("Do MMM");
};
