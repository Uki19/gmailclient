
import axios from "axios";

import { Labels } from "./actionKeys";
import Label from "../models/Label";

export const getLabels = () => {
    return async (dispatch) => {
        dispatch({ type: Labels.GET_LABELS_START });
        try {
            const response = await axios.get("labels");
            const labels = Label.fromApiList(response.data.labels);
            dispatch({
                type: Labels.GET_LABELS_SUCCESS,
                payload: {
                    labels,
                },
            });
        } catch (error) {
            dispatch({ type: Labels.GET_LABELS_ERROR });
        }
    };
};

export const selectLabel = (labelId: string) => {
    return {
        type: Labels.SELECT_LABEL,
        payload: {
            labelId,
        },
    };
};
