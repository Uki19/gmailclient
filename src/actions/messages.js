
import axios from "axios";

import Message, { LabelIds } from "../models/Message";
import { Messages, getMessagesActionForType, MessagesTypes } from "./actionKeys";

export const getInitialInboxMessages = (labelIds?: string[]) => {
    if (labelIds && labelIds.length === 0) {
        return getInitialMessagesAction([LabelIds.inbox]);
    }
    return getInitialMessagesAction(labelIds);
};

export const getInitialSentMessages = () => {
    return getInitialMessagesAction([LabelIds.sent]);
};

const getInitialMessagesAction = (labelIds: string[]) => {
    return async (dispatch) => {
        const messagesType = labelIds.includes(LabelIds.sent) ? MessagesTypes.sent
            : MessagesTypes.inbox;
        dispatch({ type: getMessagesActionForType(messagesType).start });
        try {
            const { messages, nextPageToken } = await getMessages(labelIds);
            dispatch({
                type: getMessagesActionForType(messagesType).success,
                payload: {
                    messages,
                    nextPageToken,
                },
            });
        } catch (error) {
            dispatch({ type: getMessagesActionForType(messagesType).error });
        }
    };
};

export const getNextPageSentMessages = (pageToken: string) => {
    return getNextPageMessagesAction([LabelIds.sent], pageToken);
};

export const getNextPageInboxMessages = (labelIds: string[], pageToken: string) => {
    if (labelIds && labelIds.length === 0) {
        return getNextPageMessagesAction([LabelIds.inbox], pageToken);
    }
    return getNextPageMessagesAction(labelIds, pageToken);
};

const getNextPageMessagesAction = (labelIds: string[], pageToken: string) => {
    return async (dispatch) => {
        const messagesType = labelIds.includes(LabelIds.sent) ? MessagesTypes.sent
            : MessagesTypes.inbox;
        dispatch({ type: getMessagesActionForType(messagesType).pageStart });
        try {
            const { messages, nextPageToken } = await getMessages(labelIds, pageToken);
            dispatch({
                type: getMessagesActionForType(messagesType).pageSuccess,
                payload: {
                    messages,
                    nextPageToken,
                },
            });
        } catch (error) {
            dispatch({ type: getMessagesActionForType(messagesType).pageError });
        }
    };
};

export const deleteMessage = (
    messageId: string,
) => {
    return async (dispatch) => {
        dispatch({ type: Messages.DELETE_MESSAGE_START });
        try {
            await axios.delete(`messages/${messageId}`);
            dispatch({
                type: Messages.DELETE_MESSAGE_SUCCESS,
                payload: {
                    messageId,
                },
            });
        } catch (error) {
            dispatch({ type: Messages.DELETE_MESSAGE_ERROR });
        }
    };
};

const modifyMessage = (
    messageId: string,
    addLabelIds: string[] = [],
    removeLabelIds: string[] = [],
) => {
    return async (dispatch) => {
        dispatch({ type: Messages.MODIFY_MESSAGE_START });
        try {
            const response = await axios.post(`messages/${messageId}/modify`, {
                addLabelIds,
                removeLabelIds,
            });
            const message = new Message(response.data);
            dispatch({
                type: Messages.MODIFY_MESSAGE_SUCCESS,
                payload: {
                    message,
                },
            });
        } catch (error) {
            dispatch({ type: Messages.MODIFY_MESSAGE_ERROR });
        }
    };
};

export const markAsRead = (messageId: string) => {
    return modifyMessage(messageId, [], [LabelIds.unread]);
};

export const markAsUnread = (messageId: string) => {
    return modifyMessage(messageId, [LabelIds.unread]);
};

// API calls.

const getMessages = async (
    labelIds?: string[] = null,
    pageToken?: string = null,
): Promise<{
    messages: Message[];
    nextPageToken?: string;
 }> => {
    try {
        let params = {};
        if (labelIds) {
            params = {
                ...params,
                labelIds: labelIds[0],
            };
        }
        if (pageToken) {
            params = {
                ...params,
                pageToken,
            };
        }
        const response = await axios.get("/messages", { params });
        const { messages, nextPageToken } = response.data;
        let fetchedMessages: Message[] = [];
        if (messages && messages.length > 0) {
            fetchedMessages = await Promise.all(
                messages.map(message => getMessage(message.id)),
            );
        }
        return {
            messages: fetchedMessages,
            nextPageToken,
        };
    } catch (error) {
        throw error;
    }
};

export const getMessage = async (messageId: string): Promise<Message> => {
    try {
        const response = await axios.get(`/messages/${messageId}`);
        return new Message(response.data);
    } catch (error) {
        throw error;
    }
};
