
const createActionTypesForKeys = (keys: string[]): { key: string } => {
    const actionKeys = {};
    keys.forEach((key: string) => {
        actionKeys[key] = key;
    });
    return actionKeys;
};

export const Auth: { key: string } = createActionTypesForKeys([
    "SIGNIN_SUCCESS",
    "SIGNIN_ERROR",
    "SIGNOUT_SUCCESS",
    "SIGNOUT_ERROR",
    "LOAD_AUTH_START",
    "LOAD_AUTH_FINISH",
]);

export const MessagesTypes = {
    inbox: "INBOX",
    sent: "SENT",
};

type MessagesActionType = {
    start: string;
    success: string;
    error: string;
    pageStart: string;
    pageSuccess: string;
    pageError: string;
}

export const Messages: { key: string } = createActionTypesForKeys([
    "MODIFY_MESSAGE_START",
    "MODIFY_MESSAGE_SUCCESS",
    "MODIFY_MESSAGE_ERROR",
    "DELETE_MESSAGE_START",
    "DELETE_MESSAGE_SUCCESS",
    "DELETE_MESSAGE_ERROR",
]);

export const getMessagesActionForType = (type: String): MessagesActionType => {
    return {
        start: `${type}_GET_INITIAL_MESSAGES_START`,
        success: `${type}_GET_INITIAL_MESSAGES_SUCCESS`,
        error: `${type}_GET_INITIAL_MESSAGES_ERROR`,
        pageStart: `${type}_GET_NEXT_PAGE_MESSAGES_START`,
        pageSuccess: `${type}_GET_NEXT_PAGE_MESSAGES_SUCCESS`,
        pageError: `${type}_GET_NEXT_PAGE_MESSAGES_ERROR`,
    };
};

export const Message: { key: string } = createActionTypesForKeys([
    "SELECT_MESSAGE",
    "SEND_MESSAGE_START",
    "SEND_MESSAGE_SUCCESS",
    "SEND_MESSAGE_ERROR",
]);

export const Labels: { key: string } = createActionTypesForKeys([
    "SELECT_LABEL",
    "GET_LABELS_START",
    "GET_LABELS_SUCCESS",
    "GET_LABELS_ERROR",
]);
