
import { GoogleSignin } from "react-native-google-signin";

import { Auth } from "./actionKeys";
import { Keys, setItem, getItem } from "../utils/LocalStorage";
import CurrentUser, { type UserInfo } from "../utils/CurrentUser";
import { setupAxios } from "../utils/net";

export const signIn = () => {
    return async (dispatch) => {
        try {
            const user: UserInfo = await GoogleSignin.signIn();
            CurrentUser.set(user);
            setItem(Keys.ACCESS_TOKEN, user.accessToken);
            setupAxios();
            dispatch({
                type: Auth.SIGNIN_SUCCESS,
                payload: {
                    user,
                    accessToken: user.accessToken,
                },
            });
        } catch (error) {
            dispatch({
                type: Auth.SIGNIN_ERROR,
                payload: {
                    error,
                },
            });
        }
    };
};

export const signOut = () => {
    return async (dispatch) => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            CurrentUser.set(null);
            dispatch({ type: Auth.SIGNOUT_SUCCESS });
        } catch (error) {
            dispatch({ error: Auth.SIGNOUT_ERROR });
        }
    };
};


export const loadAuth = () => {
    return async (dispatch) => {
        dispatch({ type: Auth.LOAD_AUTH_START });

        let accessToken = await getItem(Keys.ACCESS_TOKEN);
        const isSignedIn = await GoogleSignin.isSignedIn();

        // If user is logged in, setup his auth data.
        if (accessToken && isSignedIn) {
            const user: UserInfo = await GoogleSignin.signInSilently();
            CurrentUser.set(user);
            setupAxios();
        } else {
            accessToken = null;
        }
        dispatch({
            type: Auth.LOAD_AUTH_FINISH,
            payload: {
                accessToken,
            },
        });
    };
};
