import axios from "axios";

import { getEncodedMessage } from "../utils/message";
import { Message } from "./actionKeys";
import { getMessage } from "./messages";

export const selectMessage = (messageId: string) => {
    return {
        type: Message.SELECT_MESSAGE,
        payload: {
            messageId,
        },
    };
};

export const sendMessage = (
    to: string,
    subject: string,
    body: string,
    threadId?: string,
) => {
    return async (dispatch) => {
        return new Promise(async (resolve, reject) => {
            const encodedMessage = getEncodedMessage(to, subject, body);
            dispatch({ type: Message.SEND_MESSAGE_START });
            try {
                let params = {
                    raw: encodedMessage,
                };
                if (threadId) {
                    params = {
                        ...params,
                        threadId,
                    };
                }
                const response = await axios.post("messages/send", params);
                const messageId = response.data.id;
                const message = await getMessage(messageId);
                dispatch({
                    type: Message.SEND_MESSAGE_SUCCESS,
                    payload: {
                        message,
                    },
                });
                resolve();
            } catch (error) {
                dispatch({
                    type: Message.SEND_MESSAGE_ERROR,
                    payload: {
                        error,
                    },
                });
                reject();
            }
        });
    };
};
