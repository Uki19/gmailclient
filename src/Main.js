// @flow

import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";

import { type AppState } from "./store/initialState";
import LoginNavigation from "./navigation/LoginNavigation";
import MainNavigation from "./navigation/MainNavigation";
import { loadAuth } from "./actions/auth";

type DispatchProps = {
    loadAuth: () => void;
}

type StateProps = {
    accessToken: ?string;
    isLoading: boolean;
}

type Props = DispatchProps & StateProps;

class Main extends React.Component<Props> {
    componentDidMount() {
        this.props.loadAuth();
    }

    render() {
        const { accessToken, isLoading } = this.props;
        if (isLoading) {
            return <View />;
        }
        if (accessToken) {
            return <MainNavigation />;
        }
        return <LoginNavigation />;
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        accessToken: state.auth.accessToken,
        isLoading: state.auth.isLoadingAuth,
    };
};

export default connect(mapStateToProps, {
    loadAuth,
})(Main);
