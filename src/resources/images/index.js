// @flow

export const attachmentIcon = require("./attachment.png");
export const inboxTabIcon = require("./inbox.png");
export const addIcon = require("./add.png");
export const closeIcon = require("./close.png");
export const sendIcon = require("./send.png");
export const userTabIcon = require("./user.png");
export const outboxTabIcon = require("./outbox.png");
export const signOutIcon = require("./logout.png");
export const checkIcon = require("./checkmark.png");
export const labelIcon = require("./label.png");
export const replyIcon = require("./reply.png");
