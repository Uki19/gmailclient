// @flow

export const materialLoadingAnimation = require("./material_wave_loading.json");
export const bikerAnimation = require("./biking_is_cool.json");
export const reactAnimation = require("./react-logo.json");
