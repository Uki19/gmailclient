// @flow

import {
    combineReducers,
    createStore,
    applyMiddleware,
} from "redux";
// import logger from "redux-logger";
import thunk from "redux-thunk";

import auth from "../reducers/auth";
import { inbox, sent } from "../reducers/messages";
import message from "../reducers/message";
import labels from "../reducers/labels";
import { Auth } from "../actions/actionKeys";
import InitialState from "./initialState";

const appReducer = combineReducers({
    auth,
    message,
    inbox,
    sent,
    labels,
});

const rootReducer = (state, action) => {
    if (action.type === Auth.SIGNOUT_SUCCESS) {
        return InitialState;
    }
    return appReducer(state, action);
};

// const store = createStore(reducers, applyMiddleware(thunk, logger));
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
