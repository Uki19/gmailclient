// @flow

import Message from "../models/Message";
import Label from "../models/Label";

export type AppState = {
    auth: AuthState;
    message: MessageState;
    inbox: MessagesState;
    sent: MessagesState;
    labels: LabelsState;
}

export type AuthState = {
    accessToken: ?string;
    isLoadingAuth: boolean;
}

export type MessagesState = {
    messages: { [key: number]: Message };
    nextPageToken: ?string;
    isLoadingInitial: boolean;
    isLoadingNextPage: boolean;
}

export type MessageState = {
    selectedMessageId: ?string;
    isSendingMessage: boolean;
}

export type LabelsState = {
    labels: Label[];
    isLoadingLabels: boolean;
    selectedLabelIds: string[];
}

const InitialState: AppState = {
    auth: {
        accessToken: null,
        isLoadingAuth: false,
    },
    message: {
        selectedMessageId: null,
        isSendingMessage: false,
    },
    inbox: {
        messages: {},
        nextPageToken: null,
        isLoadingInitial: false,
        isLoadingNextPage: false,
    },
    sent: {
        messages: {},
        nextPageToken: null,
        isLoadingInitial: false,
        isLoadingNextPage: false,
    },
    labels: {
        labels: [],
        isLoadingLabels: false,
        selectedLabelIds: [],
    },
};

export default InitialState;
