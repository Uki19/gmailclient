// @flow

import React from "react";
import { connect } from "react-redux";
import { NavigationScreenProp, NavigationState } from "react-navigation";
import _ from "lodash";
import { View, StyleSheet } from "react-native";

import { getInitialInboxMessages, getNextPageInboxMessages } from "../actions/messages";
import { type AppState, type MessagesState } from "../store/initialState";
import Message from "../models/Message";
import MessagesList from "../components/messages/MessagesList";
import LabelsView from "../components/labels/LabelsView";
import TouchableImage from "../components/general/TouchableImage";
import { labelIcon } from "../resources/images";
import { marginDefault } from "../general/dimensions";

type DispatchProps = {
    getInitialInboxMessages: (labelIds: string[]) => void;
    getNextPageInboxMessages: (labelIds: string[], pageToken: string) => void;
    selectMessage: (messageId: string) => void;
    deleteMessage: (messageId: string) => void;
};

type ParentProps = {
    navigation: NavigationScreenProp<NavigationState>,
};

type AppStateProps = MessagesState & {
    messages: Message[];
    selectedLabelIds: string[];
};

type State = {
    isShowingLabelsFilter: boolean;
}

type Props = DispatchProps & ParentProps & AppStateProps;

class InboxScreen extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => {
        let action = null;
        if (navigation.state.params) {
            action = navigation.state.params.onFilterIconPress;
        }
        return {
            headerRight: (
                <TouchableImage
                    onPress={action}
                    source={labelIcon}
                    style={{ marginRight: marginDefault }}
                />
            ),
        };
    };

    constructor() {
        super();
        this.state = {
            isShowingLabelsFilter: false,
        };
    }

    componentWillMount() {
        this.props.navigation.setParams({ onFilterIconPress: this.onFilterIconPress });
    }

    componentDidMount() {
        this.props.getInitialInboxMessages(this.props.selectedLabelIds);
    }

    render() {
        const { isLoadingInitial, messages, isLoadingNextPage } = this.props;
        return (
            <View style={styles.container}>
                <MessagesList
                    messages={messages}
                    onLoadNextPage={this.onLoadNextPage}
                    isLoadingNextPage={isLoadingNextPage}
                    onRefresh={this.onRefresh}
                    refreshing={isLoadingInitial}
                />
                {this.state.isShowingLabelsFilter
                && (<LabelsView
                    style={StyleSheet.absoluteFill}
                    onDismiss={this.dismissFilterLabelsView}
                    onSave={this.onSaveFilters}
                />
                )}
            </View>
        );
    }

    onRefresh = () => {
        this.props.getInitialInboxMessages(this.props.selectedLabelIds);
    }

    onLoadNextPage = () => {
        const { nextPageToken, isLoadingNextPage, selectedLabelIds } = this.props;
        if (nextPageToken && !isLoadingNextPage) {
            this.props.getNextPageInboxMessages(selectedLabelIds, nextPageToken);
        }
    };

    onFilterIconPress = () => {
        if (!this.state.isShowingLabelsFilter) {
            this.setState({
                isShowingLabelsFilter: true,
            });
        }
    }

    dismissFilterLabelsView = () => {
        this.setState({
            isShowingLabelsFilter: false,
        });
    }

    onSaveFilters = () => {
        this.props.getInitialInboxMessages(this.props.selectedLabelIds);
        this.dismissFilterLabelsView();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const mapStateToProps = (state: AppState) => {
    return {
        ...state.inbox,
        messages: _.valuesIn(state.inbox.messages),
        selectedLabelIds: state.labels.selectedLabelIds,
    };
};

export default connect(
    mapStateToProps,
    {
        getInitialInboxMessages,
        getNextPageInboxMessages,
    },
)(InboxScreen);
