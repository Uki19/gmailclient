// @flow

import React, { Component } from "react";
import {
    StyleSheet,
    KeyboardAvoidingView,
    StatusBar,
    Alert,
} from "react-native";
import { connect } from "react-redux";
import { NavigationScreenProp, NavigationState } from "react-navigation";

import { sendMessage } from "../actions/message";
import { alabaster } from "../general/colors";
import NewMessageForm from "../components/message/NewMessageForm";
import NewMessageHeader from "../components/message/NewMessageHeader";
import Button from "../components/general/Button";
import LoadingIndicatorModal from "../components/general/LoadingIndicatorModal";
import { isPlatformIOS } from "../utils/device";
import { type AppState } from "../store/initialState";

type StateProps = {
    isSendingMessage: boolean;
};

type RouteParams = {
    to: string[];
    subject: string;
    threadId: string;
}

type ParentProps = {
    navigation: NavigationScreenProp<RouteParams>;
};

type DispatchProps = {
    sendMessage: (to: string, subject: string, body: string, threadId?: string) => Promise<void>;
};

type Props = ParentProps & StateProps & DispatchProps;

type State = {
    isFormValid: boolean;
};

class NewMessageScreen extends Component<Props, State> {
    messageForm: ?NewMessageForm;

    constructor() {
        super();
        this.state = {
            isFormValid: false,
        };
    }

    render() {
        let subject = "";
        let to = [];
        const { params } = this.props.navigation.state;
        if (params) {
            subject = params.subject;
            to = params.to;
        }
        return (
            <KeyboardAvoidingView
                style={styles.mainWrapper}
                behavior={isPlatformIOS ? "padding" : null}
            >
                <StatusBar barStyle="dark-content" />
                <NewMessageHeader
                    onClosePress={this.onClosePress}
                    onSendPress={this.onSendPress}
                    isSendDisabled={!this.state.isFormValid}
                />
                <LoadingIndicatorModal
                    visible={this.props.isSendingMessage}
                />
                <NewMessageForm
                    ref={(ref: ?NewMessageForm) => { this.messageForm = ref; }}
                    onValidationChange={this.onFormValidationChange}
                    subject={subject}
                    receivers={to}
                />
                <Button
                    title="Send"
                    onPress={this.onSendPress}
                    disabled={!this.state.isFormValid}
                />
            </KeyboardAvoidingView>
        );
    }

    onFormValidationChange = (isValid: boolean) => {
        this.setState({ isFormValid: isValid });
    }

    onSendPress = async () => {
        if (this.messageForm != null) {
            const { to, subject, body } = this.messageForm.getFormData();
            const { params } = this.props.navigation.state;
            const threadId = params ? params.threadId : null;
            try {
                await this.props.sendMessage(to, subject, body, threadId);
                this.props.navigation.goBack();
            } catch (error) {
                Alert.alert("Failed to send message");
            }
        }
    };

    onClosePress = () => {
        this.props.navigation.goBack();
    };
}

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        backgroundColor: alabaster,
        justifyContent: "flex-start",
    },
});

const mapStateToProps = (state: AppState): StateProps => {
    return {
        isSendingMessage: state.message.isSendingMessage,
    };
};

export default connect(
    mapStateToProps,
    {
        sendMessage,
    },
)(NewMessageScreen);
