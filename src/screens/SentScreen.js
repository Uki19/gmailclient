// @flow

import React from "react";
import { connect } from "react-redux";
import { NavigationScreenProp, NavigationState } from "react-navigation";
import _ from "lodash";

import { getInitialSentMessages, getNextPageSentMessages } from "../actions/messages";
import { type AppState, type MessagesState } from "../store/initialState";
import Message from "../models/Message";
import MessagesList from "../components/messages/MessagesList";

type DispatchProps = {
    getInitialSentMessages: () => void,
    getNextPageSentMessages: (pageToken: string) => void,
    selectMessage: (messageId: string) => void,
};

type ParentProps = {
    navigation: NavigationScreenProp<NavigationState>,
};

type AppStateProps = MessagesState & {
    messages: Message[],
};

type Props = DispatchProps & ParentProps & AppStateProps;

class SentScreen extends React.Component<Props> {
    componentDidMount() {
        this.props.getInitialSentMessages();
    }

    render() {
        const { isLoadingInitial, messages, isLoadingNextPage } = this.props;
        return (
            <MessagesList
                messages={messages}
                onLoadNextPage={this.onLoadNextPage}
                isLoadingNextPage={isLoadingNextPage}
                onRefresh={this.props.getInitialSentMessages}
                refreshing={isLoadingInitial}
            />
        );
    }

    onLoadNextPage = () => {
        const { nextPageToken, isLoadingNextPage } = this.props;
        if (nextPageToken && !isLoadingNextPage) {
            this.props.getNextPageSentMessages(nextPageToken);
        }
    };
}

const mapStateToProps = (state: AppState) => {
    return {
        ...state.sent,
        messages: _.valuesIn(state.sent.messages),
    };
};

export default connect(
    mapStateToProps,
    {
        getInitialSentMessages,
        getNextPageSentMessages,
    },
)(SentScreen);
