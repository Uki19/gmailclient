// @flow

import React, { Component } from "react";
import {
    StyleSheet, Image, StatusBar, View, Alert,
} from "react-native";
import LottieView from "lottie-react-native";
import { connect } from "react-redux"; 

import { alabaster, veryLightTangelo, dustyGray } from "../general/colors";
import CurrentUser, { type UserInfo } from "../utils/CurrentUser";
import Text from "../components/general/Text";
import { bikerAnimation } from "../resources/animations";
import { marginDefaultHalved, marginDefault } from "../general/dimensions";
import { signOut } from "../actions/auth";
import TouchableImage from "../components/general/TouchableImage";
import { signOutIcon } from "../resources/images";

const defaultProfile = "https://i.stack.imgur.com/34AD2.jpg";

type State = {
    userInfo: UserInfo,
};

type DispatchProps = {
    signOut: () => void;
}

type Props = DispatchProps;

class ProfileScreen extends Component<Props, State> {
    constructor() {
        super();
        this.state = {
            userInfo: CurrentUser.get(),
        };
    }

    render() {
        const { user } = this.state.userInfo;
        return (
            <View style={styles.mainWrapper}>
                <TouchableImage
                    source={signOutIcon}
                    style={styles.signOutButton}
                    onPress={this.onSignOutPress}
                    tintColor={dustyGray}
                />
                <View style={styles.infoWrapper}>
                    <Image
                        source={{ uri: user.photo || defaultProfile }}
                        style={[styles.profileImage]}
                    />
                    <Text style={styles.nameText}>
                        {`${user.name}`}
                    </Text>
                    <Text style={styles.emailText}>
                        {`${user.email}`}
                    </Text>
                </View>
                <LottieView
                    source={bikerAnimation}
                    style={styles.bikerView}
                    loop
                    autoPlay
                />
            </View>
        );
    }

    onSignOutPress = () => {
        Alert.alert(
            "Sign Out?",
            "Are you sure you want to sign out?",
            [
                {
                    text: "Cancel",
                    style: "cancel",
                },
                {
                    text: "Yes",
                    onPress: this.props.signOut,
                },
            ],
        );
    }
}

const PROFILE_IMAGE_SIZE = 150;

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        backgroundColor: alabaster,
        alignItems: "center",
        justifyContent: "space-around",
    },
    infoWrapper: {
        alignItems: "center",
    },
    profileImage: {
        width: PROFILE_IMAGE_SIZE,
        aspectRatio: 1,
        borderRadius: PROFILE_IMAGE_SIZE / 2,
        borderWidth: 4,
        borderColor: veryLightTangelo,
    },
    nameText: {
        fontSize: 24,
        marginTop: marginDefaultHalved,
        fontWeight: "bold",
    },
    emailText: {
        marginTop: marginDefaultHalved,
        color: dustyGray,
    },
    bikerView: {
        height: 200,
    },
    signOutButton: {
        alignSelf: "flex-end",
        marginRight: marginDefault,
    },
});

export default connect(null, {
    signOut,
})(ProfileScreen);
