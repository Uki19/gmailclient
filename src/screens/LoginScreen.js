// @flow

import React from "react";
import { Animated, View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { GoogleSigninButton } from "react-native-google-signin";
import LottieView from "lottie-react-native";

import { signIn } from "../actions/auth";
import { alabaster } from "../general/colors";
import { reactAnimation } from "../resources/animations";

const ANIMATION_DURATION = 4000;

type DispatchProps = {
    signIn: () => void;
}

type ParentProps = {
};

type Props = DispatchProps & ParentProps;

type State = {
    buttonOpacity: Animated.Value;
}

class LoginScreen extends React.Component<Props, State> {
    constructor() {
        super();
        this.state = {
            buttonOpacity: new Animated.Value(0),
        };
    }

    componentDidMount() {
        setTimeout(() => {
            Animated.timing(this.state.buttonOpacity, {
                toValue: 1,
                duration: 300,
            }).start();
        }, ANIMATION_DURATION);
    }

    render() {
        return (
            <View style={styles.mainWrapper}>
                <LottieView
                    source={reactAnimation}
                    loop={false}
                    style={{ flex: 2 }}
                    duration={ANIMATION_DURATION}
                    autoSize
                    autoPlay
                />
                <Animated.View style={{
                    flex: 1,
                    opacity: this.state.buttonOpacity,
                }}
                >
                    <GoogleSigninButton
                        style={styles.googleButton}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Light}
                        onPress={this.signIn}
                    />
                </Animated.View>
            </View>
        );
    }

    signIn = () => {
        this.props.signIn();
    }
}

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
        backgroundColor: alabaster,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    googleButton: {
        width: 312,
        height: 48,
    },
});

export default connect(null, {
    signIn,
})(LoginScreen);
