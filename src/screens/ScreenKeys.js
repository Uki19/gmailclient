// @flow

const ScreenKeys = {
    Inbox: "Inbox",
    Sent: "Sent",
    InboxTab: "InboxTab",
    SentTab: "SentTab",
    MainTabNavigation: "MainTabNavigation",
    Message: "Message",
    NewMessage: "NewMessage",
    ProfileTab: "ProfileTab",
};

export default ScreenKeys;
