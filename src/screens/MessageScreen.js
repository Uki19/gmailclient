// @flow

import React, { Component } from "react";
import {
    ScrollView,
    View,
    StyleSheet,
} from "react-native";
import { Buffer } from "buffer";
import { connect } from "react-redux";
import { NavigationScreenProp, NavigationState } from "react-navigation";

import Message from "../models/Message";
import Text from "../components/general/Text";
import {
    paddingDefault,
    marginDefaultHalved,
    paddingDefaultHalved,
    marginDefault,
} from "../general/dimensions";
import { AttachmentsList } from "../components/attachment/AttachmentsList";
import { markAsRead } from "../actions/messages";
import MessageInfoHeader from "../components/message/MessageInfoHeader";
import MessageWebView from "../components/message/MessageWebView";
import { type AppState } from "../store/initialState";
import { dustyGray } from "../general/colors";
import TouchableImage from "../components/general/TouchableImage";
import { replyIcon } from "../resources/images";
import ScreenKeys from "./ScreenKeys";

type State = {
    messageHtmlData: ?string;
    messageTextData: ?string;
};

type StateProps = {
    message: Message;
};

type ParentProps = {
    navigation: NavigationScreenProp<NavigationState>;
}

type DispatchProps = {
    markAsRead: (messageId: string) => void;
};

type Props = StateProps & ParentProps & DispatchProps;

class MessageScreen extends Component<Props, State> {
    static navigationOptions = ({ navigation }) => {
        let action = null;
        if (navigation.state.params) {
            action = navigation.state.params.onReplyPress;
        }
        return {
            headerRight: (
                <TouchableImage
                    onPress={action}
                    source={replyIcon}
                    style={{ marginRight: marginDefault }}
                />
            ),
        };
    };

    constructor() {
        super();
        this.state = {
            messageHtmlData: null,
            messageTextData: null,
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({ onReplyPress: this.onReplyPress });

        const { message } = this.props;
        if (message.isUnread) {
            this.props.markAsRead(message.id);
        }
        let messageHtmlData: ?string = null;
        let messageTextData: ?string = null;
        if (message.htmlBody) {
            messageHtmlData = Buffer.from(message.htmlBody.data, "base64").toString("utf8");
        }
        if (message.textBody) {
            messageTextData = Buffer.from(message.textBody.data, "base64").toString("utf8");
        }
        this.setState({
            messageHtmlData,
            messageTextData,
        });
    }

    render() {
        const { message } = this.props;
        return (
            <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
                <MessageInfoHeader message={message} />
                {this.renderEmailContent()}
                {message.attachments.length > 0 && (
                    <View>
                        <AttachmentsList attachments={message.attachments} />
                    </View>
                )}
            </ScrollView>
        );
    }

    renderEmailContent = () => {
        const { messageHtmlData, messageTextData } = this.state;
        if (messageHtmlData) {
            return (
                <MessageWebView
                    messageHtmlData={messageHtmlData}
                />
            );
        }
        if (messageTextData) {
            return (
                <Text style={styles.messageText}>
                    {messageTextData}
                </Text>
            );
        }
        return (
            <Text style={styles.noBodyText}>
                Message has empty body.
            </Text>
        );
    };

    onReplyPress = () => {
        const { message } = this.props;
        this.props.navigation.navigate(ScreenKeys.NewMessage, {
            subject: message.headers.subject,
            to: message.headers.from.split(", "),
            threadId: message.threadId,
        });
    }
}

const styles = StyleSheet.create({
    mainWrapper: {
        flex: 1,
    },
    scrollView: {
        flex: 1,
        backgroundColor: "white",
    },
    contentContainer: {
        paddingVertical: paddingDefault,
        paddingHorizontal: paddingDefaultHalved,
    },
    messageText: {
        marginTop: marginDefaultHalved,
    },
    noBodyText: {
        marginTop: marginDefaultHalved,
        color: dustyGray,
    },
});

const mapStateToProps = (state: AppState) => {
    const { message, inbox, sent } = state;
    let selectedMessage = {};
    if (inbox.messages[message.selectedMessageId]) {
        selectedMessage = inbox.messages[message.selectedMessageId];
    } else if (sent.messages[message.selectedMessageId]) {
        selectedMessage = sent.messages[message.selectedMessageId];
    }
    return {
        message: selectedMessage,
    };
};

export default connect(mapStateToProps, { markAsRead })(MessageScreen);
