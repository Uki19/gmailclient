/**
 * @flow
 */

import React from "react";
import { GoogleSignin } from "react-native-google-signin";
import { Provider } from "react-redux";
import { View, StatusBar } from "react-native";

import Main from "./Main";
import store from "./store";

type Props = {};

GoogleSignin.configure({
    iosClientId: "150723685900-ratpamp4qpp98hkr7ovj1eatkploh0q5.apps.googleusercontent.com",
    scopes: ["https://mail.google.com/"],
});

export default class App extends React.Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <View style={{ flex: 1 }}>
                    <StatusBar barStyle="dark-content" />
                    <Main />
                </View>
            </Provider>
        );
    }
}
