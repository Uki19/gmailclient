// @flow

import React, { Component } from "react";
import { StyleSheet, View, Modal } from "react-native";

import LoadingIndicator from "./LoadingIndicator";
import { whiteSmoke } from "../../general/colors";
import Text from "./Text";
import { borderRadiusMedium } from "../../general/dimensions";
import { globalStyles } from "../../general/styles";

type Props = {
    visible: boolean;
    text?: ?string;
    onClose?: () => void;
};

export default class LoadingIndicatorModal extends Component<Props> {
    static defaultProps = {
        text: null,
        onClose: null,
    };

    render() {
        const { visible, text } = this.props;
        return (
            <Modal
                animationType="fade"
                visible={visible}
                onRequestClose={this.onRequestClose}
                con
                transparent
            >
                <View style={styles.contentWrapper}>
                    <View style={[globalStyles.defaultShadow, styles.indicatorWrapper]}>
                        <LoadingIndicator />
                        {text && (
                            <Text>
                                {text}
                            </Text>
                        )}
                    </View>
                </View>
            </Modal>
        );
    }

    onRequestClose = () => {
        if (this.props.onClose) {
            this.props.onClose();
        }
    }
}

const styles = StyleSheet.create({
    contentWrapper: {
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.2)",
        alignItems: "center",
        justifyContent: "center",
    },
    indicatorWrapper: {
        backgroundColor: whiteSmoke,
        borderRadius: borderRadiusMedium,
    },
});
