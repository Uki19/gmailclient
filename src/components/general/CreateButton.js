// @flow

import React, { Component } from "react";
import {
    StyleSheet, TouchableOpacityProps, TouchableOpacity, Image,
} from "react-native";

import { veryLightTangelo } from "../../general/colors";
import { globalStyles } from "../../general/styles";
import { addIcon } from "../../resources/images";

type Props = TouchableOpacityProps;

export default class CreateButton extends Component<Props> {
    render() {
        return (
            <TouchableOpacity
                {...this.props}
                activeOpacity={0.9}
                style={[
                    this.props.style,
                    globalStyles.defaultShadow,
                    styles.buttonWrapper,
                ]}
            >
                <Image source={addIcon} />
            </TouchableOpacity>
        );
    }
}

const DEFAULT_BUTTON_SIZE = 60;

const styles = StyleSheet.create({
    buttonWrapper: {
        alignItems: "center",
        justifyContent: "center",
        height: DEFAULT_BUTTON_SIZE,
        aspectRatio: 1,
        backgroundColor: veryLightTangelo,
        borderRadius: DEFAULT_BUTTON_SIZE / 2,
        shadowOpacity: 0.5,
    },
});
