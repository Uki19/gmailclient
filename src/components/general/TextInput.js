// @flow

import React, { Component } from "react";
import {
    TextInput as RNTextInput, StyleSheet, TextInputProperties,
} from "react-native";

import { shark } from "../../general/colors";

export default class TextInput extends Component<TextInputProperties> {
    render() {
        return <RNTextInput {...this.props} style={[styles.defaultTextInput, this.props.style]} />;
    }
}

const styles = StyleSheet.create({
    defaultTextInput: {
        color: shark,
    },
});
