// @flow

import React, { Component } from "react";
import {
    Text as RNText, StyleSheet, TextProps,
} from "react-native";

import { shark } from "../../general/colors";

export default class Text extends Component<TextProps> {
    render() {
        return <RNText {...this.props} style={[styles.defaultText, this.props.style]} />;
    }
}

const styles = StyleSheet.create({
    defaultText: {
        color: shark,
    },
});
