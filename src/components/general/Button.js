// @flow

import React, { Component } from "react";
import {
    View, Text, StyleSheet, TouchableOpacity, ViewStyle,
} from "react-native";
import { veryLightTangelo, whiteSmoke } from "../../general/colors";
import { paddingDefaultDouble } from "../../general/dimensions";

type Props = {
    title: string;
    onPress: () => void;
    disabled?: boolean;
    style?: ViewStyle;
};

export default class Button extends Component<Props> {
    static defaultProps = {
        disabled: false,
        style: {},
    }

    render() {
        const { disabled } = this.props;
        return (
            <View style={{ opacity: disabled ? 0.6 : 1 }}>
                <TouchableOpacity
                    style={[styles.button, this.props.style]}
                    onPress={this.props.onPress}
                    disabled={this.props.disabled}
                >
                    <Text style={styles.buttonTitle}>
                        {this.props.title}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: veryLightTangelo,
        padding: paddingDefaultDouble,
    },
    buttonTitle: {
        color: whiteSmoke,
        fontSize: 16,
        fontWeight: "600",
    },
});
