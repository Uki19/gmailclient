import React, { Component } from "react";
import { StyleSheet } from "react-native";
import LottieView from "lottie-react-native";

import { materialLoadingAnimation } from "../../resources/animations";

export default class LoadingIndicator extends Component {
    render() {
        return (
            <LottieView
                source={materialLoadingAnimation}
                style={styles.animation}
                autoPlay
                loop
            />
        );
    }
}

const styles = StyleSheet.create({
    animation: {
        width: 120,
    },
});
