// @flow

import React, { Component } from "react";
import {
    TouchableOpacity,
    Image,
    View,
    TouchableOpacityProps,
    ImageSourcePropType,
} from "react-native";

type Props = TouchableOpacityProps & {
    source: ImageSourcePropType;
    tintColor?: string;
};

export default class TouchableImage extends Component<Props> {
    render() {
        const { disabled, style, tintColor } = this.props;
        return (
            <View style={[{ opacity: disabled ? 0.6 : 1 }, style]}>
                <TouchableOpacity
                    onPress={this.props.onPress}
                    activeOpacity={0.9}
                    disabled={disabled}
                >
                    <Image
                        source={this.props.source}
                        style={{ tintColor }}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}
