// @flow

import React, { Component } from "react";
import {
    StyleSheet, View, NativeSyntheticEvent, TextInputKeyPressEventData,
} from "react-native";
import TextInput from "../general/TextInput";
import { marginDefaultHalved, paddingDefaultHalved, paddingDefault } from "../../general/dimensions";
import Text from "../general/Text";
import { havelockBlue } from "../../general/colors";
import { isStringEmail } from "../../utils/strings";

type Props = {
    onReceiversChange: ?(receivers: string[]) => void;
    receivers?: string[];
};

type State = {
    text: string;
    receivers?: string[];
    selectedReceiverIndex: number;
};

export default class ReceiverInput extends Component<Props, State> {
    static defaultProps = {
        receivers: [],
    }

    constructor(props: Props) {
        super(props);
        this.state = {
            text: "",
            receivers: this.props.receivers,
            selectedReceiverIndex: -1,
        };
    }

    render() {
        return (
            <View style={styles.mainWrapper}>
                {this.renderReceivers()}
                <TextInput
                    style={styles.input}
                    placeholder="To"
                    value={this.state.text}
                    onChangeText={this.onChangeToText}
                    onSubmitEditing={this.onSubmitEditing}
                    onEndEditing={this.onSubmitEditing}
                    blurOnSubmit={false}
                    onKeyPress={this.onKeyPress}
                />
            </View>
        );
    }

    renderReceivers = () => {
        return this.state.receivers.map((receiver: string, index: number) => {
            const isSelected = index === this.state.selectedReceiverIndex;
            return (
                <View
                    key={receiver}
                    style={[
                        styles.receiverItemWrapper,
                        isSelected && styles.receiverItemWrapperSelected,
                    ]}
                >
                    <Text style={[
                        styles.receiverItemText,
                        isSelected && styles.receiverItemTextSelected,
                    ]}
                    >
                        {receiver}
                    </Text>
                </View>
            );
        });
    }

    onChangeToText = (text: string) => {
        this.setState({ text });
    };

    onKeyPress = (event: NativeSyntheticEvent<TextInputKeyPressEventData>) => {
        if (event.nativeEvent.key === "Backspace") {
            const { text, receivers, selectedReceiverIndex } = this.state;
            if (text.length === 0 && receivers.length > 0) {
                if (selectedReceiverIndex === -1) {
                    this.setState({
                        selectedReceiverIndex: receivers.length - 1,
                    });
                } else {
                    const newReceivers = receivers
                        .filter((receiver, index) => selectedReceiverIndex !== index);
                    this.setState({
                        receivers: newReceivers,
                        selectedReceiverIndex: -1,
                    }, this.onReceiversChange);
                }
            }
        }
    }

    onSubmitEditing = () => {
        const { text, receivers } = this.state;
        const trimmedText = text.trim().toLowerCase();
        if (!receivers.includes(trimmedText)
            && trimmedText.length > 0
            && isStringEmail(trimmedText)) {
            this.setState({
                receivers: [...receivers, trimmedText],
                text: "",
            }, this.onReceiversChange);
        }
    };

    onReceiversChange = () => {
        if (this.props.onReceiversChange) {
            this.props.onReceiversChange(this.state.receivers);
        }
    }
}

const TEXT_INPUT_MIN_WIDTH = 100;
const RECEIVER_ITEM_HEIGHT = 30;

const styles = StyleSheet.create({
    mainWrapper: {
        flexDirection: "row",
        alignItems: "center",
        flexWrap: "wrap",
    },
    input: {
        flex: 1,
        fontSize: 16,
        marginTop: marginDefaultHalved,
        minWidth: TEXT_INPUT_MIN_WIDTH,
    },
    receiverItemWrapper: {
        justifyContent: "center",
        paddingVertical: paddingDefaultHalved,
        paddingHorizontal: paddingDefault,
        marginRight: marginDefaultHalved,
        marginTop: marginDefaultHalved,
        borderColor: havelockBlue,
        borderWidth: 1,
        height: RECEIVER_ITEM_HEIGHT,
        borderRadius: RECEIVER_ITEM_HEIGHT / 2,
    },
    receiverItemWrapperSelected: {
        backgroundColor: havelockBlue,
    },
    receiverItemText: {
        fontSize: 13,
        color: havelockBlue,
    },
    receiverItemTextSelected: {
        color: "white",
    },
});
