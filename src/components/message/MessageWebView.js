// @flow

import React, { Component } from "react";
import {
    WebView,
    Linking,
    NativeSyntheticEvent,
    WebViewMessageEventData,
    NavState,
    Alert,
} from "react-native";
import { initialScript } from "../../utils/injectScripts";

const SET_SCROLL_MESSAGE_NAME = "SET_SCROLL_HEIGHT";

type State = {
    webViewHeight: number,
    webViewDidEndLoading: boolean,
};

type Props = {
    messageHtmlData: string,
};

export default class MessageWebView extends Component<Props, State> {
    webView: WebView;

    constructor() {
        super();
        this.state = {
            webViewHeight: 0,
            webViewDidEndLoading: false,
        };
    }

    render() {
        const { webViewHeight } = this.state;
        const { messageHtmlData } = this.props;
        return (
            <WebView
                style={{ height: webViewHeight }}
                ref={(ref: WebView) => { this.webView = ref; }}
                originWhitelist={["*"]}
                source={{ html: messageHtmlData }}
                injectedJavaScript={initialScript}
                onMessage={this.onWebViewMessage}
                onNavigationStateChange={this.onNavigationStateChange}
                onLoadEnd={this.onWebViewLoadEnd}
                startInLoadingState
                scalesPageToFit
                javaScriptEnabled
            />
        );
    }

    onWebViewMessage = (event: NativeSyntheticEvent<WebViewMessageEventData>) => {
        const data = JSON.parse(event.nativeEvent.data);
        const { name, payload } = data;
        if (name === SET_SCROLL_MESSAGE_NAME) {
            this.setState({
                webViewHeight: payload.scrollHeight,
            });
        }
    };

    onWebViewLoadEnd = () => {
        this.setState({ webViewDidEndLoading: true });
    };

    onNavigationStateChange = async (event: NavState) => {
        if (this.state.webViewDidEndLoading) {
            const canOpen = await Linking.canOpenURL(event.url);
            if (canOpen) {
                try {
                    await Linking.openURL(event.url);
                } catch (error) {
                    Alert.alert(`Unable to open url: ${event.url}`);
                }
            }
            this.webView.stopLoading();
        }
    }
}
