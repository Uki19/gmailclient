// @flow

import React, { Component } from "react";
import { StyleSheet, View, SafeAreaView } from "react-native";

import TouchableImage from "../general/TouchableImage";
import { closeIcon, sendIcon } from "../../resources/images";
import { paddingDefault } from "../../general/dimensions";
import Text from "../general/Text";

type Props = {
    onClosePress: () => void;
    onSendPress: () => void;
    isSendDisabled: boolean;
};

export default class NewMessageHeader extends Component<Props> {
    render() {
        return (
            <SafeAreaView>
                <View style={styles.header}>
                    <TouchableImage
                        source={closeIcon}
                        onPress={this.props.onClosePress}
                    />
                    <Text style={styles.title}>
                        New message
                    </Text>
                    <TouchableImage
                        disabled={this.props.isSendDisabled}
                        source={sendIcon}
                        onPress={this.props.onSendPress}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: paddingDefault,
    },
    title: {
        fontWeight: "600",
        fontSize: 16,
    },
});
