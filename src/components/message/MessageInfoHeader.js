// @flow

import React, { Component } from "react";
import { Text, StyleSheet, View } from "react-native";

import Message from "../../models/Message";
import { paddingSmall, marginDefaultHalved } from "../../general/dimensions";
import { dustyGray } from "../../general/colors";

type Props = {
    message: Message;
};

export default class MessageInfoHeader extends Component<Props> {
    render() {
        const { message } = this.props;
        return (
            <View style={styles.headerWrapper}>
                <Text style={styles.subjectText}>
                    {message.headers.subject}
                </Text>
                <Text style={styles.fromToText}>
                    {`From: ${message.headers.from}`}
                </Text>
                <Text style={styles.fromToText}>
                    {`Delivered To: ${message.headers.to}`}
                </Text>
                <Text style={styles.fromToText}>
                    {message.headers.date}
                </Text>
                <View style={styles.borderLine} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerWrapper: {
        padding: paddingSmall,
    },
    borderLine: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: dustyGray,
        marginTop: marginDefaultHalved,
    },
    subjectText: {
        fontSize: 20,
        fontWeight: "bold",
    },
    fromToText: {
        fontSize: 11,
        color: dustyGray,
        marginTop: marginDefaultHalved,
    },
    dateText: {},
});
