// @flow

import React, { Component } from "react";
import { StyleSheet, ScrollView } from "react-native";
import TextInput from "../general/TextInput";
import { paddingDefault, marginDefault } from "../../general/dimensions";
import ReceiverInput from "./ReceiverInput";

type State = {
    to: string;
    subject: string;
    body: string;
    isValid: boolean;
};

type Props = {
    onValidationChange: ?(valid: boolean) => void;
    subject?: string;
    receivers?: string[];
}

export default class NewMessageForm extends Component<Props, State> {
    static defaultProps = {
        subject: "",
        receivers: [],
    };

    constructor(props: Props) {
        super(props);
        this.state = {
            to: "",
            subject: this.props.subject ? this.props.subject : "",
            body: "",
            isValid: false,
        };
    }

    componentDidMount() {
        this.onReceiversChange(this.props.receivers || []);
        this.checkValidation();
    }

    render() {
        const { subject, body } = this.state;
        return (
            <ScrollView
                style={styles.scrollWrapper}
                contentContainerStyle={styles.scrollContentWrapper}
                keyboardShouldPersistTaps="never"
            >
                <ReceiverInput
                    onReceiversChange={this.onReceiversChange}
                    receivers={this.props.receivers}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Subject"
                    value={subject}
                    onChangeText={this.onChangeSubjectText}
                />
                <TextInput
                    style={[styles.input, styles.bodyInput]}
                    placeholder="Message"
                    value={body}
                    onChangeText={this.onChangeBodyText}
                    
                    multiline
                />
            </ScrollView>
        );
    }

    onChangeSubjectText = (text: string) => {
        this.setState({ subject: text }, this.checkValidation);
    };

    onChangeBodyText = (text: string) => {
        this.setState({ body: text }, this.checkValidation);
    };

    onReceiversChange = (receivers: string[]) => {
        this.setState({
            to: receivers.join(", "),
        }, this.checkValidation);
    }

    checkValidation = () => {
        const {
            to,
            isValid,
        } = this.state;
        const isValidNow = (to.length > 0);
        if (isValidNow !== isValid) {
            this.setState({
                isValid: isValidNow,
            });
            if (this.props.onValidationChange) {
                this.props.onValidationChange(isValidNow);
            }
        }
    }

    getFormData = (): { to: string, subject: string, body: string } => {
        return this.state;
    }
}

const styles = StyleSheet.create({
    scrollWrapper: {
        flex: 1,
        paddingVertical: paddingDefault,
    },
    scrollContentWrapper: {
        padding: paddingDefault,
        flexDirection: "column",
        justifyContent: "flex-start",
    },
    input: {
        marginTop: marginDefault,
        fontSize: 16,
    },
    bodyInput: {
        minHeight: 200,
        textAlignVertical: "top",
    },
});
