// @flow

import React, { Component } from "react";
import { FlatList } from "react-native";
import { type Attachment } from "../../models/Message";
import { AttachmentItem } from "./AttachmentItem";

type Props = {
    attachments: Attachment[];
};

export class AttachmentsList extends Component<Props> {
    render() {
        return (
            <FlatList
                data={this.props.attachments}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
                horizontal
            />
        );
    }

    keyExtractor = (item: Attachment): string => item.body.attachmentId;

    renderItem = (info: { item: Attachment }) => {
        return (
            <AttachmentItem
                attachment={info.item}
            />
        );
    }
}

export default AttachmentsList;
