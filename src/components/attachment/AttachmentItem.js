// @flow

import React, { Component } from "react";
import { View, StyleSheet, Image } from "react-native";

import { type Attachment } from "../../models/Message";
import { alabaster, gallery } from "../../general/colors";
import { paddingDefault, borderRadiusSmall, marginDefaultHalved } from "../../general/dimensions";
import Text from "../general/Text";
import { attachmentIcon } from "../../resources/images";

type Props = {
    attachment: Attachment;
}

export class AttachmentItem extends Component<Props> {
    render() {
        return (
            <View style={styles.itemWrapper}>
                <Image
                    source={attachmentIcon}
                />
                <Text style={styles.attachmentNameText}>
                    {this.props.attachment.filename}
                </Text>
            </View>
        );
    }
}

const ITEM_WIDTH = 130;
const ITEM_HEIGHT = 100;

const styles = StyleSheet.create({
    itemWrapper: {
        backgroundColor: alabaster,
        width: ITEM_WIDTH,
        height: ITEM_HEIGHT,
        padding: paddingDefault,
        borderWidth: 1,
        borderColor: gallery,
        borderRadius: borderRadiusSmall,
        justifyContent: "space-between",
        marginRight: marginDefaultHalved,

    },
    attachmentNameText: {
        fontSize: 11,
    },
});

export default AttachmentItem;
