// @flow

import React, { Component } from "react";
import {
    StyleSheet, Image, TouchableOpacity,
} from "react-native";

import Label from "../../models/Label";
import { checkIcon } from "../../resources/images";
import { paddingDefaultHalved } from "../../general/dimensions";
import Text from "../general/Text";
import { havelockBlue } from "../../general/colors";

type Props = {
    label: Label;
    isSelected: boolean;
    onPress: (label: Label) => void;
};

export default class LabelItem extends Component<Props> {
    render() {
        const {
            label,
            onPress,
            isSelected,
        } = this.props;
        return (
            <TouchableOpacity
                style={styles.itemContainer}
                onPress={() => onPress(label)}
            >
                <Text style={styles.nameText}>
                    {label.name}
                </Text>
                {isSelected && (<Image
                    source={checkIcon}
                    style={styles.checkmark}
                />
                )}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingVertical: paddingDefaultHalved,
    },
    checkmark: {
        tintColor: havelockBlue,
    },
    nameText: {
        fontSize: 16,
        color: "gray",
    },
});
