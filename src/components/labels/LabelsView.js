// @flow

import React, { Component } from "react";
import {
    StyleSheet, View, ViewProps, Animated, ScrollView, ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";

import { type AppState } from "../../store/initialState";
import Button from "../general/Button";
import { paddingDefault, borderRadiusMedium } from "../../general/dimensions";
import Label, { LabelTypeEnum } from "../../models/Label";
import Text from "../general/Text";
import { getLabels, selectLabel } from "../../actions/labels";
import LabelItem from "./LabelItem";

const LABELS_VIEW_HEIGHT = 200;
const INITIAL_POSITION = -LABELS_VIEW_HEIGHT;
const FAKE_VIEW_HEIGHT = 50;

type AppStateProps = {
    labels: Label[];
    selectedLabelIds: string[];
    isLoading: boolean;
}

type DispatchProps = {
    getLabels: () => void;
}

type Props = AppStateProps & DispatchProps & ViewProps & {
    onDismiss: () => void;
    onSave: () => void;
};

type State = {
    positionY: Animated.Value;
};

class LabelsView extends Component<Props, State> {
    constructor() {
        super();
        this.state = {
            positionY: new Animated.Value(INITIAL_POSITION),
        };
    }

    componentDidMount() {
        Animated.spring(this.state.positionY, {
            toValue: 0,
            bounciness: 5,
        }).start();
        this.props.getLabels();
    }

    render() {
        return (
            <Animated.View style={[
                {
                    backgroundColor: this.state.positionY.interpolate({
                        inputRange: [INITIAL_POSITION, 0],
                        outputRange: ["rgba(0, 0, 0, 0.0)", "rgba(0, 0, 0, 0.5)"],
                    }),
                },
                this.props.style,
            ]}
            >
                <Animated.View
                    style={[styles.labelsContainer, {
                        transform: [
                            {
                                translateY: this.state.positionY,
                            },
                        ],
                    }]}

                >
                    <View style={styles.fakeView} />
                    {this.props.isLoading && <ActivityIndicator />}
                    {this.renderLabels()}
                    <View style={styles.buttonsContainer}>
                        <Button
                            title="Save"
                            style={styles.button}
                            onPress={this.save}
                        />
                        <Button
                            title="Cancel"
                            style={styles.button}
                            onPress={this.cancel}
                        />
                    </View>
                </Animated.View>
            </Animated.View>
        );
    }

    renderLabels = () => {
        return (
            <ScrollView>
                {this.props.labels.map((label: Label) => {
                    return (
                        <LabelItem
                            key={label.id}
                            label={label}
                            isSelected={this.isLabelSelected(label)}
                            onPress={this.onLabelPress}
                        />
                    );
                })}
            </ScrollView>
        );
    }

    isLabelSelected = (label: Label) => {
        return this.props.selectedLabelIds.includes(label.id);
    }

    onLabelPress = (label: Label) => {
        this.props.selectLabel(label.id);
    }

    save = () => {
        this.animateOut(this.props.onSave);
    }

    cancel = () => {
        this.animateOut(this.props.onDismiss);
    }

    animateOut = (completion: () => void) => {
        Animated.spring(this.state.positionY, {
            toValue: INITIAL_POSITION,
            bounciness: 5,
        }).start(completion);
    }
}

const styles = StyleSheet.create({
    labelsContainer: {
        backgroundColor: "white",
        height: LABELS_VIEW_HEIGHT,
        justifyContent: "space-between",
        padding: paddingDefault,
    },
    fakeView: {
        position: "absolute",
        height: FAKE_VIEW_HEIGHT,
        top: -FAKE_VIEW_HEIGHT,
        left: 0,
        right: 0,
        backgroundColor: "white",
    },
    buttonsContainer: {
        flexDirection: "row",
        justifyContent: "space-around",
    },
    button: {
        paddingVertical: paddingDefault,
        borderRadius: borderRadiusMedium,
    },
    saveButton: {
    },
});

const mapStateToProps = (state: AppState): AppStateProps => {
    return {
        labels: state.labels.labels.filter(label => label.type === LabelTypeEnum.user),
        selectedLabelIds: state.labels.selectedLabelIds,
        isLoading: state.labels.isLoadingLabels,
    };
};

export default connect(mapStateToProps, {
    getLabels,
    selectLabel,
})(LabelsView);
