// @flow

import React, { PureComponent } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import Swipeout from "react-native-swipeout";

import Message from "../../models/Message";
import { paddingDefault, marginDefaultHalved, borderRadiusSmall } from "../../general/dimensions";
import {
    alabaster, steelBlue, dustyGray, wellRead,
} from "../../general/colors";
import Text from "../general/Text";
import { globalStyles } from "../../general/styles";

type Props = {
    message: Message,
    onMessagePress: (messageId: string) => void,
    onDeletePress: (messageId: string) => void,
};

export default class MessagesListItem extends PureComponent<Props> {
    swipeoutButtons: any[];

    constructor(props: Props) {
        super(props);
        this.swipeoutButtons = [
            {
                text: "Delete",
                backgroundColor: wellRead,
                onPress: this.onDeletePress,
            },
        ];
    }

    render() {
        const { message } = this.props;
        return (
            <Swipeout right={this.swipeoutButtons} backgroundColor={alabaster} autoClose>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.itemWrapper}
                    onPress={this.onItemPress}
                >
                    <View style={[globalStyles.defaultShadow, styles.itemContent]}>
                        <View style={styles.subjectTimeContainer}>
                            <Text
                                style={
                                    message.isUnread ? styles.subjectUnreadText : styles.subjectText
                                }
                                numberOfLines={2}
                            >
                                {message.headers.subject}
                            </Text>
                            <Text style={styles.timeText}>
                                {message.representableDate}
                            </Text>
                        </View>
                        <View style={styles.snippetUnreadContainer}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.fromText}>
                                    {message.headers.from}
                                </Text>
                                <Text style={styles.snippetText} numberOfLines={2}>
                                    {message.snippet}
                                </Text>
                            </View>
                            {message.isUnread && <View style={styles.unreadIndicator} />}
                        </View>
                    </View>
                </TouchableOpacity>
            </Swipeout>
        );
    }

    onItemPress = () => {
        this.props.onMessagePress(this.props.message.id);
    };

    onDeletePress = () => {
        this.props.onDeletePress(this.props.message.id);
    };
}

const UNREAD_INDICATOR_SIZE = 16;

const styles = StyleSheet.create({
    itemWrapper: {
        backgroundColor: alabaster,
        padding: paddingDefault,
    },
    itemContent: {
        backgroundColor: "white",
        padding: paddingDefault,
        borderRadius: borderRadiusSmall,
    },
    subjectTimeContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    snippetUnreadContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    subjectUnreadText: {
        flex: 1,
        fontSize: 16,
        fontWeight: "bold",
    },
    subjectText: {
        flex: 1,
        fontSize: 16,
    },
    timeText: {
        fontSize: 11,
        color: dustyGray,
        marginLeft: marginDefaultHalved,
    },
    fromText: {
        fontSize: 12,
        color: dustyGray,
        marginVertical: marginDefaultHalved,
    },
    snippetText: {
        fontSize: 13,
        color: dustyGray,
    },
    unreadIndicator: {
        backgroundColor: steelBlue,
        height: UNREAD_INDICATOR_SIZE,
        aspectRatio: 1,
        borderRadius: UNREAD_INDICATOR_SIZE / 2,
    },
});
