// @flow

import React, { Component } from "react";
import {
    FlatList,
    ActivityIndicator,
    StyleSheet,
    View,
    LayoutAnimation,
} from "react-native";
import { withNavigation, NavigationInjectedProps } from "react-navigation";
import { connect } from "react-redux";

import { deleteMessage } from "../../actions/messages";
import { selectMessage } from "../../actions/message";
import MessagesListItem from "./MessagesListItem";
import Message from "../../models/Message";
import { paddingDefaultDouble, paddingDefault } from "../../general/dimensions";
import { alabaster } from "../../general/colors";
import ScreenKeys from "../../screens/ScreenKeys";
import CreateButton from "../general/CreateButton";
import Text from "../general/Text";

type DispatchProps = {
    selectMessage: (messageId: string) => void;
    deleteMessage: (messageId: string) => void;
}

type ParentProps = {
    messages: Message[];
    onLoadNextPage: () => void;
    isLoadingNextPage: boolean;
    onRefresh: () => void;
    refreshing: boolean;
}

type Props = NavigationInjectedProps & DispatchProps & ParentProps;

class MessagesList extends Component<Props> {
    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.messages.length !== this.props.messages.length) {
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.props.messages.length === 0
                && !this.props.refreshing
                && this.renderNoMessages()}
                <FlatList
                    style={styles.list}
                    data={this.props.messages}
                    renderItem={this.renderInboxItem}
                    onEndReached={this.props.onLoadNextPage}
                    onEndReachedThreshold={0.3}
                    ListFooterComponent={this.renderListFooter}
                    keyExtractor={this.inboxKeyExtractor}
                    onRefresh={this.props.onRefresh}
                    refreshing={this.props.refreshing}
                />
                <CreateButton
                    onPress={() => this.props.navigation.navigate(ScreenKeys.NewMessage)}
                    style={styles.createButton}
                />
            </View>
        );
    }

    inboxKeyExtractor = (item: Message) => item.id;

    renderInboxItem = (info: { item: Message }) => {
        return (
            <MessagesListItem
                message={info.item}
                onMessagePress={this.onMessagePress}
                onDeletePress={this.onDeletePress}
            />
        );
    }

    renderNoMessages = () => {
        return (
            <View style={styles.noMessagesView}>
                <Text>
                    No messages.
                </Text>
            </View>
        );
    }

    onMessagePress = (messageId: string) => {
        this.props.selectMessage(messageId);
        this.props.navigation.navigate(ScreenKeys.Message);
    }

    onDeletePress = (messageId: string) => {
        this.props.deleteMessage(messageId);
    }

    renderListFooter = () => {
        if (this.props.isLoadingNextPage) {
            return (
                <View style={styles.listFooter}>
                    <ActivityIndicator />
                </View>
            );
        }
        return null;
    }
}

const styles = StyleSheet.create({
    listFooter: {
        paddingVertical: paddingDefaultDouble,
    },
    list: {
        backgroundColor: alabaster,
    },
    createButton: {
        position: "absolute",
        bottom: paddingDefaultDouble,
        right: paddingDefaultDouble,
    },
    noMessagesView: {
        backgroundColor: alabaster,
        padding: paddingDefault,
        alignItems: "center",
        justifyContent: "center",
    },
});

export default connect(null, {
    deleteMessage,
    selectMessage,
})(withNavigation(MessagesList));
