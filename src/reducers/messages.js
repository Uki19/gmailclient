// @flow

import InitialState, { type MessagesState } from "../store/initialState";
import { Action } from "../actions";
import {
    Messages,
    MessagesTypes,
    getMessagesActionForType,
    Message,
} from "../actions/actionKeys";
import MessageModel from "../models/Message";

export const createMessagesReducer = (type: string) => (
    state: MessagesState = InitialState.inbox,
    action: Action,
) => {
    switch (action.type) {
    case getMessagesActionForType(type).start:
        return { ...state, isLoadingInitial: true };
    case getMessagesActionForType(type).success:
        return setMessages(state, action.payload);
    case getMessagesActionForType(type).error:
        return { ...state, isLoadingInitial: false };
    case getMessagesActionForType(type).pageStart:
        return { ...state, isLoadingNextPage: true };
    case getMessagesActionForType(type).pageSuccess:
        return appendMessages(state, action.payload);
    case getMessagesActionForType(type).pageError:
        return { ...state, isLoadingNextPage: false };
    case Messages.MODIFY_MESSAGE_SUCCESS:
        return updateMessage(state, action.payload);
    case Messages.DELETE_MESSAGE_SUCCESS:
        return deleteMessage(state, action.payload);
    case Message.SEND_MESSAGE_SUCCESS:
        if (type === MessagesTypes.sent) {
            const { message } = action.payload;
            return {
                ...state,
                messages: { [message.id]: message, ...state.messages },
            };
        }
        return state;
    default:
        return state;
    }
};

const setMessages = (state, payload: any): MessagesState => {
    const messages = payload.messages.reduce((result, current) => {
        return { ...result, [current.id]: current };
    }, {});
    return {
        ...state,
        isLoadingInitial: false,
        nextPageToken: payload.nextPageToken,
        messages,
    };
};

const appendMessages = (state, payload: any): MessagesState => {
    const messages = payload.messages.reduce((result, current) => {
        return { ...result, [current.id]: current };
    }, {});
    return {
        ...state,
        isLoadingNextPage: false,
        nextPageToken: payload.nextPageToken,
        messages: { ...state.messages, ...messages },
    };
};

const updateMessage = (state, payload: any): MessagesState => {
    const { message }: { message: MessageModel } = payload;
    const messageId = message.id;
    const oldMessage: MessageModel = state.messages[messageId];
    if (!oldMessage) {
        return state;
    }
    const newMessage: MessageModel = {
        ...oldMessage,
        ...message,
    };
    const updatedMessageDict = { [messageId]: newMessage };
    return {
        ...state,
        messages: {
            ...state.messages,
            ...updatedMessageDict,
        },
    };
};

const deleteMessage = (state, payload: any): MessagesState => {
    const { messageId } = payload;
    const { [messageId]: unused, ...newMessages } = state.messages;
    return {
        ...state,
        messages: newMessages,
    };
};

export const sent = createMessagesReducer(MessagesTypes.sent);
export const inbox = createMessagesReducer(MessagesTypes.inbox);
