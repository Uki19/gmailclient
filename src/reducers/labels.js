// @flow

import InitialState, {
    type LabelsState,
} from "../store/initialState";
import {
    Action,
} from "../actions";
import { Labels } from "../actions/actionKeys";

export default (state: LabelsState = InitialState.labels, action: Action): LabelsState => {
    switch (action.type) {
    case Labels.GET_LABELS_START:
        return { ...state, isLoadingLabels: true };
    case Labels.GET_LABELS_SUCCESS:
        return {
            ...state,
            isLoadingLabels: false,
            labels: action.payload.labels,
        };
    case Labels.GET_LABELS_ERROR:
        return { ...state, isLoadingLabels: false };
    case Labels.SELECT_LABEL:
        return selectLabel(state, action);
    default:
        return state;
    }
};

const selectLabel = (state: LabelsState, action: Action): LabelsState => {
    const { labelId } = action.payload;
    const { selectedLabelIds } = state;
    let newSelectedLabelIds = [];
    if (selectedLabelIds.includes(labelId)) {
        newSelectedLabelIds = selectedLabelIds.filter(lId => labelId !== lId);
    } else {
        newSelectedLabelIds = [...selectedLabelIds, labelId];
    }
    return {
        ...state,
        selectedLabelIds: newSelectedLabelIds,
    };
};
