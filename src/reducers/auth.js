// @flow

import InitialState, {
    type AuthState,
} from "../store/initialState";
import {
    Action,
} from "../actions";
import { Auth } from "../actions/actionKeys";

export default (state: AuthState = InitialState.auth, action: Action): AuthState => {
    switch (action.type) {
    case Auth.SIGNIN_SUCCESS:
        return {
            ...state,
            accessToken: action.payload.accessToken,
        };
    case Auth.LOAD_AUTH_START:
        return { ...state, isLoadingAuth: true };
    case Auth.LOAD_AUTH_FINISH:
        return {
            ...state,
            isLoadingAuth: false,
            accessToken: action.payload.accessToken,
        };
    case Auth.SIGNOUT_SUCCESS:
        return {
            ...state,
            accessToken: null,
        };
    default:
        return state;
    }
};
