// @flow

import InitialState, {
    type MessageState,
} from "../store/initialState";
import {
    Action,
} from "../actions";
import { Message } from "../actions/actionKeys";

export default (state: MessageState = InitialState.message, action: Action): MessageState => {
    switch (action.type) {
    case Message.SELECT_MESSAGE:
        return { ...state, selectedMessageId: action.payload.messageId };
    case Message.SEND_MESSAGE_START:
        return { ...state, isSendingMessage: true };
    case Message.SEND_MESSAGE_SUCCESS:
    case Message.SEND_MESSAGE_ERROR:
        return { ...state, isSendingMessage: false };
    default:
        return state;
    }
};
